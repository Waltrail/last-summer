﻿using LastSummer.Components.States;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;

namespace LastSummer
{
    public class LastSummer : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Song intro;
        Song main;
        public static int ScreenWight { get; internal set; }
        public static int ScreenHight { get; internal set; }
        
        public static Random random;
        public static int backbufferWidth;
        public static int backbufferHeight;
        public static Vector2 scale;
        private State _currentState;
        private State _nextState;

        public void ChangeState(State state)
        {
            _nextState = state;
        }

        public LastSummer()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

       
        protected override void Initialize()
        {
            backbufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            backbufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            graphics.PreferredBackBufferHeight = 1080;
            graphics.PreferredBackBufferWidth = 1920;            
            graphics.HardwareModeSwitch = true;
            graphics.IsFullScreen = true;
            graphics.SynchronizeWithVerticalRetrace = true;
            graphics.ApplyChanges();
            ScreenHight = graphics.PreferredBackBufferHeight;
            ScreenWight = graphics.PreferredBackBufferWidth;
            random = new Random();

            scale = new Vector2((float)backbufferWidth /
                (float)GraphicsDevice.PresentationParameters.BackBufferWidth,
                (float)backbufferHeight /
                (float)GraphicsDevice.PresentationParameters.BackBufferHeight);

            _nextState = new StoryState(this, graphics.GraphicsDevice, Content);
            IsMouseVisible = false;            
            base.Initialize();
        }

       
        protected override void LoadContent()
        {
            this.intro = Content.Load<Song>("Sound/intro");
            this.main = Content.Load<Song>("Sound/main");
            spriteBatch = new SpriteBatch(GraphicsDevice);
            MediaPlayer.Play(intro);
            MediaPlayer.IsRepeating = false;
            if(!(_currentState is StoryState))
            {
                MediaPlayer.MediaStateChanged += MediaPlayer_MediaStateChanged;
            }
            
        }
        void MediaPlayer_MediaStateChanged(object sender, EventArgs e)
        {            
            MediaPlayer.Volume -= 0.1f;
            MediaPlayer.Play(main);
        }

        protected override void UnloadContent()
        {
           
        }

        protected override void Update(GameTime gameTime)
        {
            if (_currentState is StoryState && Keyboard.GetState().IsKeyDown(Keys.Escape))
                MediaPlayer.Stop();
            if (_nextState != null)
            {
                _currentState = _nextState;
                _nextState = null;
            }
            _currentState.Update(gameTime);           
            base.Update(gameTime);            
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(new Color(12,9,5));
            _currentState.Draw(gameTime, spriteBatch);
            base.Draw(gameTime);
        }
    }
}
