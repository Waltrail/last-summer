﻿using System;

namespace LastSummer
{  
    public static class Program
    {       
        [STAThread]
        static void Main()
        {
            using (var game = new LastSummer())
                game.Run();
        }
    }
}