﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastSummer.Components.Models

{
    public class Sprite : ICloneable
    {
        protected Texture2D rectex;
        public bool State;
        public Texture2D _texture;
        public AnimationManager animationManager;
        protected Dictionary<string, Animation> animations;
        public Vector2 position;
        public bool IsRemoved = false;
        public float LiveSpan = 0f;

        public bool ShowRectangle { get; set; }
        public virtual Rectangle Rectangle
        {
            get
            {
                if (_texture != null)
                {
                    return new Rectangle((int)position.X, (int)position.Y, _texture.Width, _texture.Height);
                }
                if (animations != null)
                {
                    if (animationManager._animation == null)
                    {
                        return new Rectangle((int)position.X, (int)position.Y, animations.First().Value.Texture.Width / animations.First().Value.FrameCount, animations.First().Value.Texture.Height);
                    }
                    else
                    {
                        return new Rectangle((int)position.X, (int)position.Y, animationManager._animation.Texture.Width / animationManager._animation.FrameCount, animationManager._animation.Texture.Height);
                    }
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
            }
        }
        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                if (animationManager != null)
                {
                    animationManager.Position = position;
                }
                if (animationManager != null)
                {
                    animationManager.Position = position;
                }
            }
        }

        public Sprite(Texture2D texture)
        {
            _texture = texture;
            ShowRectangle = false;
        }
        public Sprite(GraphicsDevice graphicsDevice,Texture2D texture):this(texture)
        {
            SetRectangleTexture(graphicsDevice, texture);
            ShowRectangle = true;
        }

        protected void SetRectangleTexture(GraphicsDevice graphicsDevice, Texture2D texture)
        {
            var Colors = new List<Color>();
            for (int y = 0; y < texture.Height; y++)
            {
                for (int x = 0; x < texture.Width; x++)
                {
                    if(x == 0 || y == 0 || x == texture.Width - 1 || y == texture.Height - 1)
                    {
                        Colors.Add(new Color(255, 10, 10,255));
                    }
                    else
                    {
                        Colors.Add(new Color(0, 0, 0, 0));
                    }
                }
            }
            rectex = new Texture2D(graphicsDevice, texture.Width, texture.Height);
            rectex.SetData<Color>(Colors.ToArray());
        }

        public Sprite(Dictionary<string, Animation> _animations)
        {
            animations = _animations;
            animationManager = new AnimationManager(animations.First().Value);
        }
        public virtual void Update(GameTime gameTime, List<Sprite> sprites)
        {
            if (animations != null)
            {
                SetAnimation();
                animationManager.Update(gameTime);
            }
        }

        protected virtual void SetAnimation()
        {
            animationManager.Play(animations.First().Value);
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, position, Color.White);
            }
            else if (animationManager != null)
            {
                animationManager.Draw(spriteBatch);
            }
            else
            {
                throw new Exception("There's no animations or sprites");
            }
            if (ShowRectangle)
            {
                if (rectex != null)
                {
                    spriteBatch.Draw(rectex, position, Color.White);
                }
            }
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}
