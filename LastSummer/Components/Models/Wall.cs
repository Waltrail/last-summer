﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastSummer.Components.Models
{
    public class Wall : GameObject
    {
        public float Health;
        public Wall(Texture2D texture) : base(texture)
        {
            Health = 100;
        }

        public Wall(Dictionary<string, Animation> animations) : base(animations)
        {
            Health = 1000;
        }

        public Wall(GraphicsDevice graphicsDevice, Texture2D texture) : base(graphicsDevice, texture)
        {
            Health = 1000;
        }
        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            base.Update(gameTime, sprites);
            foreach(var sprite in sprites)
            {
                if(sprite is Bullet && IsTouchingLeft(sprite) && IsTouchingRight(sprite))
                {
                    Health -= ( 1 + (float) LastSummer.random.NextDouble()) * 100;
                }
            }
            if(Health <= 0)
            {
                IsRemoved = true;
            }
        }
    }
}
