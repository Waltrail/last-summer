﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastSummer.Components.Models
{
    public class Box : GameObject
    {

        private float FallTime = 0;
        private float FallSpeed = 19;
        protected bool OnGround { get; set; }

        public Box(GraphicsDevice graphicsDevice, Texture2D texture) : base(texture)
        {
            SetRectangleTexture(graphicsDevice, texture);
        }


        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            OnGround = false;
            foreach (var sprite in sprites)
            {
                if (sprite == this)
                {
                    continue;
                }
                if (IsTouchingTop(sprite))
                {
                    OnGround = true;
                }                
            }
            if (!OnGround)
            {
                FallTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (FallTime != 19)
                {
                    FallSpeed++;
                }
            }
            else
            {
                FallTime = 0;
            }
            Velocity.Y = FallSpeed * FallTime;
            position += Velocity;            
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, base.position, Color.White);
            }
            else
            {
                throw new Exception("There's no animations or sprites");
            }
            if (ShowRectangle)
            {
                if (rectex != null)
                {
                    spriteBatch.Draw(rectex, position, Color.White);
                }
            }
        }
    }
}
