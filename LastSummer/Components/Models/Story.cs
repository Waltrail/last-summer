﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastSummer.Components.Models
{
    public class Story : Sprite
    {
        private bool PlayedHalf;
        public bool AllPlayed;
        private float timer;
        public bool One;
        public Story(Texture2D texture) : base(texture)
        {
            PlayedHalf = false; AllPlayed = false; timer = 0f;
        }

        public Story(Dictionary<string, Animation> _animations) : base(_animations)
        {
            PlayedHalf = false; AllPlayed = false; timer = 0f;
        }

        public Story(GraphicsDevice graphicsDevice, Texture2D texture) : base(graphicsDevice, texture)
        {
            PlayedHalf = false; AllPlayed = false; timer = 0f;
        }
        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            if (animations != null)
            {
                if (animations != null)
                {
                    SetAnimation();
                    animationManager.Update(gameTime);
                    if (!One)
                    {
                        if (animationManager._animation.CurrentFrame == animationManager._animation.FrameCount - 1 && PlayedHalf)
                        {
                            AllPlayed = true;
                        }
                        if (animationManager._animation.CurrentFrame == animationManager._animation.FrameCount - 1)
                        {
                            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                            if (timer > animationManager._animation.Speed)
                                PlayedHalf = true;
                        }
                    }
                }
            }
        }

        protected override void SetAnimation()
        {
            if (animations != null)
            {
                if (!PlayedHalf)
                {
                    animationManager.Play(animations.First().Value);
                }
                else
                {
                    animationManager.Play(animations["second"]);
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, position, Color.White);
            }
            else if (animationManager != null)
            {
                animationManager.Draw(spriteBatch);
            }
            else
            {
                throw new Exception("There's no animations or sprites");
            }            
        }

    }
}
