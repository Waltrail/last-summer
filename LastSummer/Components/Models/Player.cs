﻿using LastSummer.Components.Controls;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LastSummer.Components.Models
{
    public class Player : GameObject
    {
        public Bullet Fireball;
        public Input input;
        protected float JumpSpeed;
        protected bool OnGround;
        protected AnimationManager animationManagerHead;             
        private float FallTime = 0;
        private float FallSpeed = 19;
        private bool Attack;
        public bool Grab;
        public bool Ball;
        private KeyboardState PrefKey;
        private KeyboardState CurrKey;
        public new Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
                if (animationManager != null)
                {
                    animationManager.Position = position;
                }                
            }
        }
        public override Rectangle Rectangle

        {
            get
            {
                if (_texture != null)
                {
                    return new Rectangle((int)position.X, (int)position.Y, _texture.Width, _texture.Height);
                }
                if (animations != null)
                {
                    if (animationManager._animation == null)
                    {
                        return new Rectangle((int)position.X+60, (int)position.Y, animations.First().Value.Texture.Width / animations.First().Value.FrameCount-120, 250);
                    }
                    else
                    {
                        return new Rectangle((int)position.X+60, (int)position.Y, animationManager._animation.Texture.Width / animationManager._animation.FrameCount-120, 250);
                    }
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
            }
        }
        public Player(Texture2D texture) : base(texture)
        {
            input = new Input();
            Ball = false;
        }
        public Player(GraphicsDevice graphicsDevice,Texture2D texture) : base(texture)
        {
            SetRectangleTexture(graphicsDevice, Rectangle);
            input = new Input();
            Ball = false;
        }

        public Player(Dictionary<string, Animation> _animations) : base(_animations)
        {

            animations = _animations;
            animationManager = new AnimationManager(animations.First().Value)
            {
                Position = Position + new Vector2(0, 160)
            };
            animationManagerHead = new AnimationManager(animations.First().Value)
            {
                Position = Position
            };
            input = new Input();
            Ball = false;
        }
        public Player(GraphicsDevice graphicsDevice,Dictionary<string, Animation> _animations) : base(_animations)
        {
            SetRectangleTexture(graphicsDevice, Rectangle);
            animations = _animations;
            animationManager = new AnimationManager(animations.First().Value)
            {
                Position = Position + new Vector2(0, 160)
            };
            animationManagerHead = new AnimationManager(animations.First().Value)
            {
                Position = Position
            };
            input = new Input();
            Ball = false;
        }

        private void SetRectangleTexture(GraphicsDevice graphicsDevice, Rectangle rectangle)
        {
            var Colors = new List<Color>();
            for (int y = 0; y < rectangle.Height; y++)
            {
                for (int x = 0; x < rectangle.Width; x++)
                {
                    if (x == 0 || y == 0 || x == rectangle.Width - 1 || y == rectangle.Height - 1)
                    {
                        Colors.Add(new Color(255, 10, 10, 255));
                    }
                    else
                    {
                        Colors.Add(new Color(0, 0, 0, 0));
                    }
                }
            }
            rectex = new Texture2D(graphicsDevice, rectangle.Width, rectangle.Height);
            rectex.SetData<Color>(Colors.ToArray());
        }

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            ShotFireball(sprites);
            Move();
            OnGround = false;
            Grab = false;
            foreach (var sprite in sprites)
            {
                if (sprite == this)
                {
                    continue;
                }
                if (sprite is Bullet)
                {
                    continue;
                }
                if (sprite is Ball)
                {
                    if (IsTouchingTop(sprite) || !IsTouchingBottom(sprite) || IsTouchingLeft(sprite) || IsTouchingRight(sprite))
                        Grab = true;
                    continue;
                }
                if (sprite is Generator)
                    continue;
                if (IsTouchingTop(sprite) && !IsTouchingBottom(sprite))
                {
                    OnGround = true;
                }
                if (IsTouchingBottom(sprite))
                {
                    FallSpeed = 0;
                }
                if ((Velocity.X > 0 && IsTouchingLeft(sprite) || Velocity.X < 0 && IsTouchingRight(sprite)))
                {
                    Velocity.X = 0;
                }
            }
            FallTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (!OnGround)
            {
                FallTime += (float)gameTime.ElapsedGameTime.TotalSeconds;
                if (FallTime != 19)
                {
                    FallSpeed++;
                }
            }
            else
            {
                FallTime = 0;
                FallSpeed = 0;
            }
            Jump();

            Velocity.Y = FallSpeed * FallTime;
            SetAnimation();
            if (animations != null)
            {
                animationManager.Update(gameTime);
                animationManagerHead.Update(gameTime);
                animationManager.Position = Position + new Vector2(0, 160);
                animationManagerHead.Position = Position;
            }
            position += Velocity;

        }        

        private void ShotFireball(List<Sprite> sprites)
        {
            PrefKey = CurrKey;
            CurrKey = Keyboard.GetState();
            if (CurrKey.IsKeyDown(input.Attack) && PrefKey.IsKeyUp(input.Attack))
            {
                animationManagerHead.Play(animations[key: "attack"]);
                Attack = true;
            }
            if (animationManager._animation != null)
            {
                if (animationManagerHead._animation.CurrentFrame == animationManagerHead._animation.FrameCount-1 && animationManagerHead._animation == animations["attack"])
                {
                    var fireball = Fireball.Clone() as Bullet;
                    fireball.animationManager.Play(animations["Fireball"]);
                    fireball.Speed = 30;
                    fireball.LiveSpan = 5;
                    fireball.Position = Position + new Vector2(Rectangle.Width / 2+10, Rectangle.Height / 2-40);
                    if (animationManager.Flip)
                    {
                        fireball.Direction = -1;
                        fireball.animationManager.Flip = true;
                    }
                    else
                    {
                        fireball.Direction = 1;
                        fireball.animationManager.Flip = false;
                    }
                    sprites.Add(fireball);
                    Attack = false;
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_texture != null)
            {
                spriteBatch.Draw(_texture, base.position, Color.White);
            }
            else if(animationManager != null && animationManagerHead != null)
            {
                animationManager.Draw(spriteBatch);
                animationManagerHead.Draw(spriteBatch);
            }
            else
            {
                throw new Exception("There's no animations or sprites");
            }

            if (rectex != null)
            {
                spriteBatch.Draw(rectex, position + new Vector2(60,0), Color.White);
            }
            
        }

        public void SetColor(Color color)
        {
            animationManager.Color = color;
        }

        protected override void SetAnimation()
        {
            if (animations != null)
            {
                if (Velocity.X > 0)
                {
                    animationManager.Flip = false;
                    animationManagerHead.Flip = false;
                    if (!Attack)
                        animationManagerHead.Play(animations[key: "WalkTop"]);
                    animationManager.Play(animations[key: "WalkBottom"]);
                }
                else if (Velocity.X < 0)
                {
                    animationManager.Flip = true;
                    animationManagerHead.Flip = true;
                    if (!Attack)
                        animationManagerHead.Play(animations[key: "WalkTop"]);
                    animationManager.Play(animations[key: "WalkBottom"]);
                }
                else if (Velocity.X == 0)
                {
                    if (!Attack)
                        animationManagerHead.Play(animations[key: "IdleTop"]);
                    animationManager.Play(animations[key: "IdleBottom"]);
                }                
            }
        }

        private void Move()
        {
            if (Keyboard.GetState().IsKeyDown(input.Crouch))
            {
               
            }
            if (Keyboard.GetState().IsKeyDown(input.Left))
            {
                Velocity.X = -Speed;                
            }
            else if(Keyboard.GetState().IsKeyDown(input.Right))
            {
                Velocity.X = Speed;                
            }
            else
            {
                Velocity.X = 0;                
            }            
        }
        private void Jump()
        {
            if (Keyboard.GetState().IsKeyDown(input.Jump) && OnGround)
            {
                FallSpeed = -17;
                if (!(Speed > 50))
                    Speed += 10f;
                FallTime = 1;
            }
            if (!Keyboard.GetState().IsKeyDown(input.Jump) && OnGround || Velocity.X == 0)
            {
                Speed = 10;
            }
        }
    }
}

