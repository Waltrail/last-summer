﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastSummer.Components.Models
{
    public class Ball : GameObject
    {
        
        public Ball(Texture2D texture) : base(texture)
        {            
        }
        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            base.Update(gameTime, sprites);            
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!IsRemoved)
            {
                if (_texture != null)
                {
                    spriteBatch.Draw(_texture, position, Color.White);
                }
                else if (animationManager != null)
                {
                    animationManager.Draw(spriteBatch);
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
                if (ShowRectangle)
                {
                    if (rectex != null)
                    {
                        spriteBatch.Draw(rectex, position, Color.White);
                    }
                }
            }
        }
    }
}
