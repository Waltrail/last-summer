﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastSummer.Components.Models
{
    class Generator : GameObject
    {
       
        public Generator(Texture2D texture) : base(texture)
        { 
            State = false;
        }
        public Generator(Dictionary<string, Animation> animations) : base(animations)
        {
            State = false;
        }
        protected override void SetAnimation()
        {
            if (!State)
            {
                animationManager.Play(animations.First().Value);
            }
            else
            {
                animationManager.Play(animations.Last().Value);
            }
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!IsRemoved)
            {
                if (_texture != null)
                {
                    spriteBatch.Draw(_texture, position, Color.White);
                }
                else if (animationManager != null)
                {
                    animationManager.Draw(spriteBatch);
                }
                else
                {
                    throw new Exception("There's no animations or sprites");
                }
                if (ShowRectangle)
                {
                    if (rectex != null)
                    {
                        spriteBatch.Draw(rectex, position, Color.White);
                    }
                }
            }
        }
    }
}
