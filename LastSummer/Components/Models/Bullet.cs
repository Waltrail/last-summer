﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace LastSummer.Components.Models
{
    public class Bullet : GameObject
    {
        public float timer;
        public float Direction { get; set; }

        public Bullet(Texture2D texture) : base(texture)
        {
        }

        public Bullet(Dictionary<string, Animation> _animations) : base(_animations)
        {
        }

        public Bullet(GraphicsDevice graphicsDevice, Texture2D texture) : base(graphicsDevice, texture)
        {
        }
        

        public override void Update(GameTime gameTime, List<Sprite> sprites)
        {
            timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
            foreach (var sprite in sprites) {
                if (sprite is Player || sprite is Bullet)
                    continue;
                if ((IsTouchingBottom(sprite) || IsTouchingLeft(sprite) || IsTouchingRight(sprite) || IsTouchingTop(sprite))&&!(sprite is Wall))
                {                    
                    IsRemoved = true;
                }
            }
            if(timer > LiveSpan)
            {
                IsRemoved = true;
            }
            Velocity.X = Direction;
            
            if(Speed < 1000)
            {
                Speed += 2;
            }
            Position += Velocity * Speed;
            animationManager.Update(gameTime);
        }
    }
}
