﻿using LastSummer.Components.Controls;
using LastSummer.Components.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LastSummer.Components.States
{
    public class Menustate : State
    {

        private List<Button> _components;
        private List<Sprite> _sprites;
        private Sprite Options;
        private bool ShowOptions;
        private bool PrevDown = Keyboard.GetState().IsKeyDown(Keys.Down);
        private bool PrevUp = Keyboard.GetState().IsKeyDown(Keys.Up);
        Matrix scaleMatrix;        

        public Menustate(LastSummer game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            scaleMatrix = Matrix.CreateScale(LastSummer.scale.X, LastSummer.scale.Y, 1);
            var PlayTex = content.Load<Texture2D>("Sprites/Menu/play_button");
            var OptionsTex = content.Load<Texture2D>("Sprites/Menu/options_button");
            var QuitTex = content.Load<Texture2D>("Sprites/Menu/quit_button");
            var PlayButton = new Button(PlayTex)
            {
                position = new Vector2(LastSummer.ScreenWight / 2 - PlayTex.Width / 2, 350)
            };
            var OptionsButton = new Button(OptionsTex)
            {
                position = new Vector2(LastSummer.ScreenWight / 2 - PlayTex.Width / 2, 450)
            };
            var QuitButton = new Button(QuitTex)
            {
                position = new Vector2(LastSummer.ScreenWight / 2 - PlayTex.Width / 2, 550)
            };
            _components = new List<Button>()
            {
                PlayButton,
                OptionsButton,
                QuitButton,
            };
            PlayButton.Click += NewGameButtonClick;
            QuitButton.Click += QuitButtonClick;
            OptionsButton.Click += OptionsButtonClick;
            PlayButton.Hover(true);
            ShowOptions = false;

            var MenuBack = content.Load<Texture2D>("Sprites/Menu/menu");
            var Menu = new Sprite(MenuBack) { Position = new Vector2(LastSummer.ScreenWight/2 - MenuBack.Width / 2, 0)};
            var LogoAnimation = new Dictionary<string, Animation>() {
                { "logo",new Animation(content.Load<Texture2D>("Sprites/Menu/logo"), 4) }
            };
            var Logo = new Sprite(LogoAnimation) { Position = new Vector2(LastSummer.ScreenWight / 2 - LogoAnimation.First().Value.Texture.Width / LogoAnimation.First().Value.FrameCount / 2, 170) };

            var chill = content.Load<Texture2D>("Sprites/Menu/chill");
            var Lark = new Sprite(chill) { Position = new Vector2(LastSummer.ScreenWight / 2 - chill.Width / 2, 700) };
            var options = content.Load<Texture2D>("Sprites/Menu/menu_options");
            Options = new Sprite(options) { Position = new Vector2(LastSummer.ScreenWight / 2 - MenuBack.Width / 2, 0) };            
            var bonefire = new Dictionary<string, Animation>() {
                { "fire",new Animation(content.Load<Texture2D>("Sprites/Menu/fire"), 5)  }
            };
            var Fire = new Sprite(bonefire) { Position = new Vector2(LastSummer.ScreenWight / 2 - bonefire.First().Value.Texture.Width / bonefire.First().Value.FrameCount / 2, 805) };
            _sprites = new List<Sprite>()
            {                
                Menu,
                Logo,
                Lark,
                Fire,
            };            
        }

        private void OptionsButtonClick(object sender, EventArgs e)
        {
            ShowOptions = true;
        }

        private void QuitButtonClick(object sender, EventArgs e)
        {
            _game.Exit();
        }

        private void NewGameButtonClick(object sender, EventArgs e)
        {
            _game.ChangeState(new GameState1(_game, _graphicsDevice, _content));
        }


        public override void Update(GameTime gameTime)
        {

            if (!ShowOptions)
            {
                if (!PrevUp && Keyboard.GetState().IsKeyDown(Keys.Up))
                {
                    for (int i = 0; i < _components.Count; i++)
                    {
                        if (_components[i].isHover)
                        {
                            _components[i].isHover = false;
                            if (i == 0)
                            {
                                _components[2].isHover = true;
                            }
                            else
                            {
                                _components[i - 1].isHover = true;
                            }
                            break;
                        }
                    }
                }
                PrevUp = Keyboard.GetState().IsKeyDown(Keys.Up);
                if (!PrevDown && Keyboard.GetState().IsKeyDown(Keys.Down))
                {
                    for (int i = 0; i < _components.Count; i++)
                    {
                        if (_components[i].isHover)
                        {

                            _components[i].isHover = false;
                            if (i == 2)
                            {
                                _components[0].isHover = true;
                            }
                            else
                            {
                                _components[i + 1].isHover = true;
                            }
                            break;
                        }
                    }
                }

                PrevDown = Keyboard.GetState().IsKeyDown(Keys.Down);
                foreach (var sprite in _sprites)
                {
                    sprite.Update(gameTime, _sprites);
                }
                foreach (var component in _components)
                {
                    component.Update(gameTime);
                }
            }
            if(ShowOptions && Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                ShowOptions = false;
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(0, null, null, null, null, null, scaleMatrix);
            foreach (var sprite in _sprites)
            {
                sprite.Draw(gameTime, spriteBatch);
            }
            
            foreach (var component in _components)
            {
                component.Draw(gameTime, spriteBatch);
            }
            if (ShowOptions)
            {
                Options.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();
        }
    }
}
