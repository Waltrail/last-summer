﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections;

namespace LastSummer.Components.States
{
    public abstract class State
    {
        protected ContentManager _content;
        protected GraphicsDevice _graphicsDevice;
        protected LastSummer _game;        

        public abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
        public abstract void Update(GameTime gameTime);        

        public State(LastSummer game, GraphicsDevice graphicsDevice, ContentManager content)
        {
            _game = game; _graphicsDevice = graphicsDevice; _content = content;
        }
    }
}
