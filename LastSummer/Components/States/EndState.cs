﻿using System;
using System.Collections.Generic;
using LastSummer.Components.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LastSummer.Components.States
{
    public class EndState : State
    {
        private Story sstory;
        private float timer;
        Viewport originalViewport;
        Matrix scaleMatrix;
        public EndState(LastSummer game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            originalViewport = graphicsDevice.Viewport;
            graphicsDevice.Viewport = new Viewport((LastSummer.backbufferWidth - originalViewport.Width) / 2, (LastSummer.backbufferHeight - originalViewport.Height) / 2, originalViewport.Width, originalViewport.Height);
            scaleMatrix = Matrix.CreateScale(LastSummer.scale.X, LastSummer.scale.Y, 1);
            var story = new Dictionary<string, Animation>() {
                { "first",new Animation(content.Load<Texture2D>("Sprites/end"), 4) { IsLooping = true } }                
            };
            sstory = new Story(story) { Position = new Vector2(0, 0), One = true };
            timer = 0f;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin(0, null, SamplerState.PointClamp, null, null, null, scaleMatrix);
            sstory.Draw(gameTime, spriteBatch);
            spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            sstory.Update(gameTime, null);
            
            if (Keyboard.GetState().IsKeyDown(Keys.Escape) || Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                _game.Exit();
            }
        }
    }
}

