﻿using System.Collections.Generic;
using LastSummer.Components.Controls;
using LastSummer.Components.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LastSummer.Components.States
{
    class GameState2 : State
    {
        private Player player;
        private Camera camera;
        private Sprite E;    
        private List<Sprite> Touchable;
        private List<Sprite> UnTouchable;
        private KeyboardState PrefKey;
        private KeyboardState CurrKey;
        private bool Switch;
        private bool Ebool;

        public GameState2(LastSummer game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            Switch = false; Ebool = false;
            var Lark = new Dictionary<string, Animation>() {
                { "WalkTop",new Animation(content.Load<Texture2D>("Sprites/Player/run_Head"), 4) },
                { "attack",new Animation(content.Load<Texture2D>("Sprites/Player/Attack"), 6){IsLooping=false } },
                { "WalkBottom", new Animation(content.Load<Texture2D>("Sprites/Player/run_Legs"), 4)},
                { "IdleTop", new Animation(content.Load<Texture2D>("Sprites/Player/Idel"), 1)},
                { "IdleBottom", new Animation(content.Load<Texture2D>("Sprites/Player/idel_bot"), 3)},
                { "Fireball", new Animation(content.Load<Texture2D>("Sprites/Fireball"), 5){ IsLooping = true } },
    };
            player = new Player(Lark)
            {
                input = new Input()
                {
                    Up = Keys.Up,
                    Down = Keys.Down,
                    Right = Keys.D,
                    Left = Keys.A,
                    Jump = Keys.Space,
                    Attack = Keys.F
                },
                Position = new Vector2(50, 220),
                Speed = 10,
                Fireball = new Bullet(Lark)
            };
            var ballTex = content.Load<Texture2D>("Sprites/SecondRoom/ball");
            var blueTex = content.Load<Texture2D>("Sprites/SecondRoom/BlueSprite");
            var greenTex = content.Load<Texture2D>("Sprites/SecondRoom/GreenSprite");
            var mainRoomTex = content.Load<Texture2D>("Sprites/SecondRoom/mainroomBackground");
            var boneFireRoom = content.Load<Texture2D>("Sprites/SecondRoom/RoomBonefire");
            var holeTex = content.Load<Texture2D>("Sprites/SecondRoom/tonnelTex");
            var houseTex = content.Load<Texture2D>("Sprites/SecondRoom/House");
            var colorTex = content.Load<Texture2D>("Sprites/SecondRoom/Colorsthin");
            var laderTex = content.Load<Texture2D>("Sprites/SecondRoom/Laders");
            var brokeLader1Tex = content.Load<Texture2D>("Sprites/SecondRoom/BrokeLader1");
            var brokeLader2Tex = content.Load<Texture2D>("Sprites/SecondRoom/BrokeLader2");
            var Etex = content.Load<Texture2D>("Sprites/e");
            var generator = new Dictionary<string, Animation>() {
                { "Generator",new Animation(content.Load<Texture2D>("Sprites/SecondRoom/Generator"), 1) { IsLooping = false } },
                { "GeneratorOn",new Animation(content.Load<Texture2D>("Sprites/SecondRoom/GeneratortOn"), 1) { IsLooping = false } }
        };
            var brokePlat1Tex = content.Load<Texture2D>("Sprites/SecondRoom/BrokePlat1");
            var brokePlat2Tex = content.Load<Texture2D>("Sprites/SecondRoom/BrokePlat2");
            var floorTex = content.Load<Texture2D>("Sprites/FirstRoom/HoleFloor");
            var mainRoomFirstFloorTex = content.Load<Texture2D>("Sprites/SecondRoom/mainRoomFirstFloor");
            var houseFloorTex = content.Load<Texture2D>("Sprites/SecondRoom/HouseFloor");
            var mainPlatTex = content.Load<Texture2D>("Sprites/SecondRoom/mainPlat");
            var floorbonefireTex = content.Load<Texture2D>("Sprites/SecondRoom/floorbonefire");
            var mainroomTex = content.Load<Texture2D>("Sprites/SecondRoom/mainroom");
            var boxTex = content.Load<Texture2D>("Sprites/SecondRoom/bhbox");
            var wall = content.Load<Texture2D>("Sprites/SecondRoom/Wall");

            var EnBall = new Ball(ballTex) { Position = new Vector2(4190, -250) };
            var Blue = new Sprite(blueTex);
            var Green = new Sprite(greenTex);
            
            var MainRoom = new Sprite(mainRoomTex) { Position = new Vector2(800, -2770) };
            var BoneFireRoom = new Sprite(boneFireRoom) { Position = new Vector2(7780, -1140) };
            var Hole = new Sprite(holeTex) { Position = new Vector2(0,-3660) };
            var Hole1 = new Sprite(holeTex) { Position = new Vector2(-800, -3660) };
            var Hole2 = new Sprite(holeTex) { Position = new Vector2(-1600, -3660) };
            var House = new Sprite(houseTex);
            var ColorWall = new Sprite(colorTex) { Position = new Vector2(7120, -1120) };
            var Lader = new Sprite(laderTex) { Position = new Vector2(2500, -50) };
            var BrokeLader1 = new Sprite(brokeLader1Tex) { Position = new Vector2(1980,290) };
            var BrokeLader2 = new Sprite(brokeLader2Tex) { Position = new Vector2(2230, 100) };
            E = new Sprite(Etex) { Position = new Vector2(0,0) };
            var Generator = new Generator(generator) { Position = new Vector2(4120, 490) };

            var Wall = new Wall(wall) { Position = new Vector2(6700, -1120) };
            var BrokePlat1 = new GameObject(brokePlat1Tex) { Position = new Vector2(1920, 240) };
            var BrokePlat2 = new GameObject(brokePlat2Tex) { Position = new Vector2(2190, 90) };
            var HoleFloor = new GameObject(floorTex) { Position = new Vector2(0, 470) };
            var HoleFloor1 = new GameObject(floorTex) { Position = new Vector2(-800, 470) };
            var HoleFloor2 = new GameObject(floorTex) { Position = new Vector2(-1600, 470) };
            var MainRoomFirstFloor = new GameObject(mainRoomFirstFloorTex) { Position = new Vector2(800, 470) };
            var HouseFloor = new GameObject(houseFloorTex);
            var MainPlat = new GameObject(mainPlatTex) { Position = new Vector2(2440, -80) };
            var FloorBonefire = new GameObject(floorbonefireTex) { Position = new Vector2(7780, 640) };
            var MainroomFloor = new GameObject(mainroomTex) { Position = new Vector2(820, 640) };
            var Box = new GameObject(boxTex) { Position = new Vector2(1300, 340) };
            var Box1 = new GameObject(boxTex) { Position = new Vector2(1800, 340) };
            
            camera = new Camera();
            Touchable = new List<Sprite>
            {
                HoleFloor,
                HoleFloor1,
                MainRoomFirstFloor,
                MainroomFloor,
                BrokePlat1,
                BrokePlat2,
                Box,
                Box1,
                MainPlat,
                FloorBonefire,
                EnBall,                
                Generator,
                Wall,
                player
            };
            UnTouchable = new List<Sprite>
            {
                MainRoom,
                Hole,
                Hole1,
                Hole2,
                HoleFloor2,
                ColorWall,
                Lader,
                BrokeLader1,
                BrokeLader2,                
                BoneFireRoom                
            };
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            _game.GraphicsDevice.Clear(Color.Black);
            spriteBatch.End();
            spriteBatch.Begin(transformMatrix: camera.Transform);
            foreach (var sprite in UnTouchable)
            {
                sprite.Draw(gameTime, spriteBatch);
            }
            foreach (var sprite in Touchable)
            {
                sprite.Draw(gameTime, spriteBatch);
            }
            if (Ebool)
            {
                E.Draw(gameTime, spriteBatch);
            }
            spriteBatch.End();
            
        }

        public override void Update(GameTime gameTime)
        {
            Ebool = false;
            PrefKey = CurrKey;
            CurrKey = Keyboard.GetState();
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                _game.ChangeState(new Menustate(_game, _graphicsDevice, _content));
            }
            camera.Update(gameTime);
            camera.Follow(player);             

            foreach (var sprite in Touchable.ToArray())
            {
                if (sprite is Generator && (player.IsTouchingLeft(sprite) || player.IsTouchingRight(sprite) || player.IsTouchingTop(sprite)))
                {
                    E.Position = new Vector2(4150, 350);
                    Ebool = true;
                    if (player.Ball && (player.Ball && CurrKey.IsKeyDown(Keys.E) && PrefKey.IsKeyUp(Keys.E)))
                    {
                        sprite.State = true; Switch = true;
                    }
                }
                if (sprite is Wall && Switch)
                {
                    sprite.IsRemoved = true;
                }

                if (sprite is Ball && (player.IsTouchingLeft(sprite) || player.IsTouchingRight(sprite) || player.IsTouchingTop(sprite)))
                {
                    E.Position = new Vector2(4200, -380);
                    Ebool = true;
                    if (player.Grab && CurrKey.IsKeyDown(Keys.E) && PrefKey.IsKeyUp(Keys.E))
                    {
                        sprite.IsRemoved = true;
                        player.Grab = false;
                        player.Ball = true;
                    }
                }
                sprite.Update(gameTime, Touchable);
            }
            for(int i = 0; i<Touchable.Count; i++)
            {
                if (Touchable[i].IsRemoved)
                {
                    Touchable.RemoveAt(i);
                    i--;
                }
            }
            if (player.Position.X < -600)
            {
                player.Speed = -player.Speed;
                if(player.Position.Y > 2000)
                {
                    _game.ChangeState(new TestState(_game, _graphicsDevice, _content));
                }
            }   
            if( player.Position.X > 11750)
            {
                _game.ChangeState(new EndState(_game,_graphicsDevice,_content));
            }
        }
    }
}
