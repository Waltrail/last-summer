﻿using System.Collections.Generic;
using LastSummer.Components.Controls;
using LastSummer.Components.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace LastSummer.Components.States
{
    class GameState1 : State
    {        
        public Player player;
        private Camera camera;
        private List<Sprite> Touchable;
        private List<Sprite> UnTouchable;
        private Color color;
        private Texture2D white;
        public Vector2 PlayerPosition;
        
        public GameState1(LastSummer game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            color = new Color(Color.Transparent,0);           
            PlayerPosition = new Vector2(50, 220);            
            white = content.Load<Texture2D>("Sprites/white");
            var Lark = new Dictionary<string, Animation>() {
                { "WalkTop",new Animation(content.Load<Texture2D>("Sprites/Player/run_Head"), 4) },
                { "attack",new Animation(content.Load<Texture2D>("Sprites/Player/Attack"), 6){IsLooping=false } },
                { "WalkBottom", new Animation(content.Load<Texture2D>("Sprites/Player/run_Legs"), 4)},
                { "IdleTop", new Animation(content.Load<Texture2D>("Sprites/Player/Idel"), 1)},
                { "IdleBottom", new Animation(content.Load<Texture2D>("Sprites/Player/idel_bot"), 3)},
                { "Fireball", new Animation(content.Load<Texture2D>("Sprites/Fireball"), 5){ IsLooping = true } },
    };
            player = new Player(Lark)
            {
                input = new Input()
                {
                    Up = Keys.Up,
                    Down = Keys.Down,
                    Right = Keys.D,
                    Left = Keys.A,
                    Jump = Keys.Space,
                    Attack = Keys.F
                },                
                Speed = 10,
                Fireball = new Bullet(Lark)
            };
            player.Position = PlayerPosition;
            var floorTex = content.Load<Texture2D>("Sprites/FirstRoom/HoleFloor");
            var doorTex = content.Load<Texture2D>("Sprites/FirstRoom/HoleDoor");
            var backgroundTex = content.Load<Texture2D>("Sprites/FirstRoom/HoleBackground");
            var hidenTex = content.Load<Texture2D>("Sprites/FirstRoom/Hiden");
            var Hiden = new Sprite(hidenTex) { Position = new Vector2(-1210+40,-480)};
            var background = new Sprite(backgroundTex) { Position = new Vector2(0, 0) };
            var background1 = new Sprite(backgroundTex) { Position = new Vector2(800, 0) };
            var background2 = new Sprite(backgroundTex) { Position = new Vector2(1600, 0) };
            var background3 = new Sprite(backgroundTex) { Position = new Vector2(2400, 0) };
            var background4 = new Sprite(backgroundTex) { Position = new Vector2(3200, 0) };
            var background5 = new Sprite(backgroundTex) { Position = new Vector2(4000, 0) };
            var background6 = new Sprite(backgroundTex) { Position = new Vector2(4800, 0) };
            var background7 = new Sprite(backgroundTex) { Position = new Vector2(5600, 0) };
            var Door = new GameObject(doorTex) { Position = new Vector2(0, 40) };
            var Floor = new GameObject(floorTex) { Position = new Vector2(0, 470) };
            var Floor1 = new GameObject(floorTex) { Position = new Vector2(800, 470) };
            var Floor2 = new GameObject(floorTex) { Position = new Vector2(1600, 470) };
            var Floor3 = new GameObject(floorTex) { Position = new Vector2(2400, 470) };
            var Floor4 = new GameObject(floorTex) { Position = new Vector2(3200, 470) };
            var Floor5 = new GameObject(floorTex) { Position = new Vector2(4000, 470) };
            var Floor6 = new GameObject(floorTex) { Position = new Vector2(4800, 470) };
            var Floor7 = new GameObject(floorTex) { Position = new Vector2(5600, 470) };
            camera = new Camera();
            Touchable = new List<Sprite>
            {
                Floor,
                Floor1,
                Floor2,
                Floor3,
                Floor4,
                Floor5,
                Floor6,
                Floor7,
                Door,
                player
            };
            UnTouchable = new List<Sprite>
            {
                Hiden,
                background,
                background1,
                background2,
                background3,
                background4,
                background5,
                background6,
                background7
            };
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            _game.GraphicsDevice.Clear(Color.Black);
            spriteBatch.End();
            spriteBatch.Begin(transformMatrix: camera.Transform);
            foreach (var sprite in UnTouchable)
            {
                sprite.Draw(gameTime, spriteBatch);
            }
            foreach (var sprite in Touchable)
            {
                sprite.Draw(gameTime, spriteBatch);
            }            
            spriteBatch.End();
            spriteBatch.Begin();
            spriteBatch.Draw(white,new Vector2(0,0),color);
            spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                _game.ChangeState(new Menustate(_game, _graphicsDevice, _content));
            }           
            camera.Update(gameTime);
            camera.Follow(player);
            foreach (var sprite in Touchable.ToArray())
            {
                sprite.Update(gameTime, Touchable);
            }
            int alpha = (int)player.Position.X / 23;
            if (alpha > 254)
            {
                alpha = 255;
            }
            color = new Color(Color.Transparent, alpha);
            if (player.Position.X > 6300)
            {
                _game.ChangeState(new GameState2(_game, _graphicsDevice, _content));
            }
        }
    }
}
