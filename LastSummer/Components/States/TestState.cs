﻿using LastSummer.Components.Controls;
using LastSummer.Components.Models;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace LastSummer.Components.States
{
    public class TestState : State
    {
        private List<Sprite> sprites;       
        private Camera camera;
        private Player player;
        Sprite Sky;
        Sprite Cat;
        public TestState(LastSummer game, GraphicsDevice graphicsDevice, ContentManager content) : base(game, graphicsDevice, content)
        {
            var Lark = new Dictionary<string, Animation>() {
                { "WalkTop",new Animation(content.Load<Texture2D>("Sprites/Player/run_Head"), 4) },
                { "attack",new Animation(content.Load<Texture2D>("Sprites/Player/Attack"), 6)},
                { "WalkBottom", new Animation(content.Load<Texture2D>("Sprites/Player/run_Legs"), 4)},
                { "IdleTop", new Animation(content.Load<Texture2D>("Sprites/Player/Idel"), 1)},
                { "IdleBottom", new Animation(content.Load<Texture2D>("Sprites/Player/idel_bot"), 3)},
    };

            var sky = content.Load<Texture2D>("Sprites/sky");
            var ground = content.Load<Texture2D>("Sprites/ground");
            var box = content.Load<Texture2D>("Sprites/box");
            var cat = content.Load<Texture2D>("Sprites/Cat");
            Cat = new Sprite(cat) { Position = new Vector2(1950, 0) };
            var Box = new Box(graphicsDevice, box) { Position = new Vector2(300,140)};
            var Box2 = new Box(graphicsDevice, box) { Position = new Vector2(300, 240) };
            var Box3 = new Box(graphicsDevice, box) { Position = new Vector2(600, 140) };
            var Ground = new GameObject(ground) { Position = new Vector2(0, 750) };
            var Ground1 = new GameObject(ground) { Position = new Vector2(1950, 700) };
            Sky = new Sprite(sky);
            player = new Player(graphicsDevice,Lark) { input = new Input()
            {
                Up = Microsoft.Xna.Framework.Input.Keys.Up,
                Down = Microsoft.Xna.Framework.Input.Keys.Down,
                Right = Microsoft.Xna.Framework.Input.Keys.D,
                Left = Microsoft.Xna.Framework.Input.Keys.A,
                Jump = Microsoft.Xna.Framework.Input.Keys.Space
            },
                Position = new Vector2(100, 100),
                Speed = 10
            };
            camera = new Camera();
            sprites = new List<Sprite>
            {                
                Ground1,
                Ground,
                Box,                
                Box3,
                player
                
            };
        }     

        public override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                _game.ChangeState(new Menustate(_game, _graphicsDevice, _content));
            }
            camera.Update(gameTime);
            camera.Follow(player);
            foreach (var sprite in sprites)
            {
                sprite.Update(gameTime,sprites);
            }
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();
            Sky.Draw(gameTime, spriteBatch);
            spriteBatch.End();
            spriteBatch.Begin(transformMatrix: camera.Transform);
            Cat.Draw(gameTime, spriteBatch);
            foreach (var sprite in sprites)
            {
                sprite.Draw(gameTime, spriteBatch);
            }
            
            spriteBatch.End();            
        }
    }
}
