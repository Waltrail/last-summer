﻿using LastSummer.Components.Models;
using Microsoft.Xna.Framework;

namespace LastSummer.Components.Controls
{
    public class Camera
    {
        public Matrix Transform { get; private set; }
        public Matrix scaleMatrix;
        private Vector2 move;        
        private bool X;
        private bool Y;
        public Camera()
        {
            move = new Vector2(LastSummer.random.Next(-15, 15), LastSummer.random.Next(-15, 15));
            scaleMatrix = Matrix.CreateScale(LastSummer.scale.X, LastSummer.scale.Y, 1);
        }
        public void Follow(Sprite target)
        {
            var Position = Matrix.CreateTranslation(-target.position.X - (target.Rectangle.Width / 2),
                -target.position.Y - (target.Rectangle.Height / 2), 0);

            
            

            var Offset = Matrix.CreateTranslation(LastSummer.ScreenWight / 2 + move.X, LastSummer.ScreenHight / 2 + move.Y + 150, 0);

            
            Transform = Position * Offset;
            Transform = Transform * scaleMatrix;
        }
        public void Follow(Vector2 target)
        {
            var Position = Matrix.CreateTranslation(-target.X,-target.Y,0);
            var Offset = Matrix.CreateTranslation(LastSummer.ScreenWight / 2 + move.X, LastSummer.ScreenHight / 2 + move.Y + 150, 0);
            Transform = Position * Offset;
        }
        public void Update(GameTime gameTime)
        {
            if (move.X <= -8)
            {
                X = true;
            }
            if (move.X >= 8)
            {
                X = false;
            }
            if (X)
            {
                move.X += 0.02f;
            }
            else
            {
                move.X -= 0.02f;
            }

            if (move.Y <= -5)
            {
                Y = true;
            }
            if (move.Y >= 5)
            {
                Y = false;
            }
            if (Y)
            {
                move.Y += 0.02f;
            }
            else
            {
                move.Y -= 0.02f;
            }
        }
    }
}
