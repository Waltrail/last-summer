﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace LastSummer.Components.Controls
{
    public class Button : Component
    {
        
        public bool isHover;         
        private Texture2D _texture;
        public event EventHandler Click;
        public Color color { get; set; }
        
        public Vector2 position { get; set; }
        public Rectangle rectangle
        {
            get
            {
                return new Rectangle((int)position.X, (int)position.Y, _texture.Width, _texture.Height);
            }
        }
        public Button(Texture2D texture)
        {
            _texture = texture;
            color = Color.Gray;
            isHover = false;
        }
        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var _color = Color.White;
            if (isHover)
            {
                _color = Color.Yellow;
            }
            spriteBatch.Draw(_texture, rectangle, _color);
        }
        public void Hover(bool hover)
        {
            isHover = hover;
        }
        public override void Update(GameTime gameTime)
        {
            if (isHover)
            {                
                if (Keyboard.GetState().IsKeyDown(Keys.Space) || Keyboard.GetState().IsKeyDown(Keys.Enter))
                {
                    Click?.Invoke(this, new EventArgs());
                }
            }
        }
    }
}
