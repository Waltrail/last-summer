﻿using Microsoft.Xna.Framework.Input;

namespace LastSummer.Components.Controls
{
    public class Input
    {
        public Keys Up;
        public Keys Down;
        public Keys Left;
        public Keys Right;
        public Keys Jump;
        public Keys Crouch;
        public Keys Attack;
    }
}
